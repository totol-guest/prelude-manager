Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: prelude-manager
Upstream-Contact: Thomas Andrejak <thomas.andrejak@gmail.com>
Source: https://www.prelude-siem.org

Files: *
Copyright: 1998-2020 CS GROUP - France Prelude Team <support.prelude@c-s.fr>
License: GPL-2+

Files: m4/*
Copyright: 1996-2014 Free Software Foundation, Inc.
License: LGPL-2.1+

Files: m4/ax_check_link_flag.m4 m4/ax_check_compile_flag.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2011 Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+

Files: libmissing/*
Copyright: 2005-2018 Free Software Foundation, Inc.
License: LGPL-2+

Files: libmissing/float.c libmissing/float.in.h libmissing/mem* libmissing/itold.c libmissing/stdlib.in.h libmissing/verify.h
Copyright: 2005-2006, 2009-2018 Free Software Foundation, Inc.
License: LGPL-3

Files: libmissing/Makefile* libmissing/tests/*
Copyright: 2009-2018 Free Software Foundation, Inc.
License: GPL-3+

Files: libev/*
Copyright: 2007-2015 Marc Alexander Lehmann <libev@schmorp.de>
License: GPL-2+

Files: install-sh
Copyright: 1994 X Consortium
License: MIT

Files: debian/*
Copyright: 2005 Mickael Profeta <profeta@debian.org>
           2017-2020 Thomas Andrejak <thomas.andrejak@gmail.com>
License: GPL-1

License: MIT
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to
   deal in the Software without restriction, including without limitation the
   rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
   sell copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   .
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   .
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
   X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
   AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
   TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   .
   Except as contained in this notice, the name of the X Consortium shall not
   be used in advertising or otherwise to promote the sale, use or other deal-
   ings in this Software without prior written authorization from the X Consor-
   tium.

License: GPL-3+
   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, either version 3 of the License, or (at your
   option) any later version.
   .
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.
   .
   You should have received a copy of the GNU General Public License
   along with this package; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU General Public License
   version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2.1 of
   the License, or (at your option) any later version.
   .
   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.
   .
   You should have received a copy of the GNU Lesser General Public License
   along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU Lesser General Public License
   version 2.1 can be found in the file
   `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2+
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.
   .
   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.
   .
   You should have received a copy of the GNU Lesser General Public License
   along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU Lesser General Public License
   version 2.1 can be found in the file
   `/usr/share/common-licenses/LGPL-2'.

License: LGPL-3
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 3 of
   the License.
   .
   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.
   .
   You should have received a copy of the GNU Lesser General Public License
   along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU Lesser General Public License
   version 2.1 can be found in the file
   `/usr/share/common-licenses/LGPL-3'.

License: GPL-2+
   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 2 of the license, or (at your option)
   any later version.
   .
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.
   .
   You should have received a copy of the GNU General Public License
   along with this package; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU General Public License
   version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: GPL-1
   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; version 1.
   .
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.
   .
   You should have received a copy of the GNU General Public License
   along with this package; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
   USA
   .
   On Debian systems, the full text of the GNU General Public License
   version 1 can be found in the file `/usr/share/common-licenses/GPL-1'.
